import React, { Component } from 'react';

class Greeter extends Component {
  constructor(props) {
    super(props);
    this.state = {newName: this.props.name};
  }

  changeName(event) {
    let newName = event.target.value;
    // this.props.setName(newName);
    this.setState({ newName });
  }

  render() {
    return (
      <div className="Greeter">
        <span>Hello, {this.props.name}</span>
        <br/>
        <label>
          Set name:
          <input type="text"
                 value={this.state.newName}
                 onChange={this.changeName.bind(this)}
          />
        </label>
        <button onClick={() => this.props.setName(this.state.newName)}
                disabled={this.state.newName.length === 0}>
          Update Name</button>
      </div>
    );
  }
}

export default Greeter;
