import React, { Component } from 'react';

class RepFinder extends Component {
  constructor(props) {
    super(props);
    this.state = {postalCode: "", reps: []};
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    // ALWAYS need to check for state not changing, so we avoid an infinite recursion
    // i.e. in this case, if the postal codes are the same before &
    // now, we don't want to re-run the fetch, because that also sets
    // state, which means this function will get called again, ad
    // infinitium
    if (prevState.postalCode === this.state.postalCode) { return; }
    if (this.state.postalCode.length === 6) {
      fetch(`https://represent.opennorth.ca/postcodes/${this.state.postalCode}/`)
        .then(r => r.json())
        .then(js => {
          let reps = js.representatives_centroid;
          this.setState({reps});
          return true;
        })
        .catch(err => console.error("Error fetching", err));
    }
  }

  setPostal = (event) => {
    let newPostal = event.target.value.toUpperCase().replace(/\s+/g, '');
    this.setState({postalCode: newPostal});
  }

  render() {
    return (
      <div>
        <label>
          Postal Code
          <input type="text" value={this.state.postalCode} onChange={this.setPostal}/>
        </label>
        <h2>Reps for {this.state.postalCode}</h2>
        <ul>
          {this.state.reps.map((r) => <li key={r.email}>{r.elected_office} {r.first_name} - {r.email}</li>)}
        </ul>
      </div>
    );
  }
}

export default RepFinder;
