import React, { Component } from 'react';
import './App.css';
import Greeter from './Greeter.jsx';
import Clock from './Clock.jsx';
import RepFinder from './Reps.jsx';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {name: "James", foo: "bar", showClock: true};
    // this.setName = this.setName.bind(this);
  }

  // setName(newName) {
  //   this.setState({name: newName});
  // }

  // setFoo(newFoo) {
  //   this.setState({foo: newFoo});
  // }

  makeHandler(key) {
    return (newValue) => {
      this.setState({[key]: newValue});
    };
  }

  // setName = this.makeHandler('name');
  // setFoo = this.makeHandler('foo');

  /*
  setName = (newName) => {
    // ...
  }
  */

  maybeShowClock() {
    if (this.state.showClock) {
      return (<Clock/>)
    } else {
      return (<span>No Clock</span>)
    }
  }

  render() {
    return (
      <div className="App" style={{fontSize: "3em"}}>
        <Greeter name={this.state.name} setName={this.makeHandler('name')}/>
        <div>{this.state.foo}</div>
        <button onClick={() => {
          this.setState((state, props) => {
            state.foo = state.foo + "!";
            return state;
          });
        }}>Change foo</button>
        {this.maybeShowClock()}
        <button onClick={() =>
                         this.setState((state) => {
                           state.showClock = !state.showClock;
                           return state;
                         })
                        }>
          Toggle Clock</button>
        <RepFinder/>
      </div>
    );
  }
}

export default App;
