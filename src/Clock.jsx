import React, { Component } from 'react';

class Clock extends Component {
  constructor(props) {
    super(props);
    this.state = {time: new Date()};
  }

  componentDidMount() {
    console.log("mounting clock");
    this.clockInterval = setInterval(
      () => {
        // console.log("interval");
        this.setState({time: new Date()});
      },
      1000
    );
  }

  componentWillUnmount() {
    console.log("removing clock");
    clearInterval(this.clockInterval);
  }

  render() {
    return (
      <div>
        It is now {this.state.time.toLocaleString()}
      </div>);
  }
}

export default Clock;
